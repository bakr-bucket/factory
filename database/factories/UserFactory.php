<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Employee;
use App\Factory;
use App\Line;
use App\Machine;
use App\Product;
use App\Review;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '20150012', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Factory::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'sales' => $faker->numberBetween(1000, 20000),
    ];
});
$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph(1),
    ];
});


$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price'=>$faker->numberBetween(10,100),
        'description' => $faker->paragraph(1),
        'factory_id'=>Factory::all()->random()->id,
        'category_id'=>Category::all()->random()->id,
    ];
});

$factory->define(Line::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'material'=>$faker->numberBetween(100,1000),
        'breakdown'=>$faker->numberBetween(0,1),
        'description' => $faker->paragraph(1),
        'product_id'=>Product::all()->random()->id,
    ];
});

$factory->define(Machine::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'model' => $faker->name,
        'speed'=>$faker->numberBetween(10,100),
        'status'=>$faker->numberBetween(0,1),
        'line_id'=>Line::all()->random()->id,
    ];
});

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'shiftFrom' => $faker->date(),
        'shiftTo' => $faker->date(),
        'line_id'=>Line::all()->random()->id,
        'user_id'=>User::all()->random()->id,
    ];
});





$factory->define(Review::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(1),
        'product_id'=>Product::all()->random()->id,
        'user_id'=>User::all()->random()->id,
    ];
});

