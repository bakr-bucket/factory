<?php

use App\Category;
use App\Employee;
use App\Factory;
use App\Line;
use App\Machine;
use App\Product;
use App\Review;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Factory::truncate();
        Product::truncate();
        Line::truncate();
        Machine::truncate();
        Employee::truncate();
        Review::truncate();
        Category::truncate();


        $user=50;
        $factory=3;
        $product=60;
        $line=120;
        $machine=240;
        $employee=240;
        $review=240;
        $category=10;


        factory(User::class,$user)->create();
        factory(Factory::class,$factory)->create();
        factory(Category::class,$category)->create();
        factory(Product::class,$product)->create();
        factory(Line::class,$line)->create();
        factory(Machine::class,$machine)->create();
        factory(Employee::class,$employee)->create();
        factory(Review::class,$review)->create();

    }
}
