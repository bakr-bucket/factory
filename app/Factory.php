<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;

class Factory extends Model
{
    use Userstamps,SoftDeletes;
    protected $fillable = ['name', 'sales'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
