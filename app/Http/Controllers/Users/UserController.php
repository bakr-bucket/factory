<?php

namespace App\Http\Controllers\Users;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {


        return response()->json([
            'data' => $user->get(['id','name','email']),
            'status' => 200
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $roles = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
        ];
        $this->validate($request, $roles);
        $data = $request->all();
        $data['password']=bcrypt(20150012);
        $data['created_by']=User::first()->id;
        $user = User::create($data);

        return response()->json([
            'data' =>$user
        ], 200);
    }


    /**
     * Display the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json([
            'data' => $user
        ], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('email')) {
            $user->email = $request->email;
        }
        $user->updated_by=$user->id;
        $user->save();
        return response()->json([
            'data' => $user
        ], 202);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $by= $user->deleted_by=$user->id;
        $user->delete();

        return response()->json([
            'data' => $by
        ], 201);
    }

    public function restore(User $user)
    {
        $by=$user->deleted_by;
        $user->restore();

        return response()->json([
            'data' => $by
        ], 201);
    }
}
