<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;
class Review extends Model
{
    use Userstamps
        ,SoftDeletes;
    protected $fillable=[
        'description',
        'user_id',
        'product_id'
        ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
