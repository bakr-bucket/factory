<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;

class Machine extends Model
{
    use Userstamps,SoftDeletes;
    //
    protected $fillable=['name','model','speed','status','line_id'];

    public function line(){
        return $this->belongsTo(Line::class);
    }

}
