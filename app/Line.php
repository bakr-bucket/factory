<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;
class Line extends Model
{
    use Userstamps,SoftDeletes;

    protected $fillable=['name','material','breakdown','description','product_id'];

    public function machines(){
        return $this->hasMany(Machine::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }


}
