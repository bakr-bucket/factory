<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;
class Product extends Model
{
    use  Userstamps,SoftDeletes;
    protected $fillable=[
        'name',
        'price',
        'description',
        'factory_id',
        'category_id'
    ];


    public function factory(){
        return $this->belongsTo(Factory::class);
    }
    public function lines(){
        return $this->hasMany(Line::class);
    }
    public function reviews(){
        return $this->hasMany(Review::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }


}
