<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
//use Mazyvan\Userstamps\Userstamps;
use Wildside\Userstamps\Userstamps;
class Employee extends Model
{
    use Userstamps,SoftDeletes;
    //
    use HasRoles;
    protected $fillable = [
        'user_id',
        'line_id',
        'shiftFrom',
        'shiftTo'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function line()
    {
        return $this->belongsTo(Line::class);
    }


}
