<?php

use App\User;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return response()->json([
        'data' =>'hello world',
        'status' => 200
    ]);
});

Route::resource('users', 'Users\UserController');

Route::get('users/restore/{user}', 'Users\UserController@restore')
    ->name('user.restore');
// BINDING TRASHED ENTITY
Route::bind('user', function ($id) {
    return User::withTrashed()->find($id);
});

